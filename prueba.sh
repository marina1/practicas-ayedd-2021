#!/bin/bash


#
# update local repo
#
{

    git pull -X theirs origin master
    git pull -X theirs origin rama-alumno

    git checkout rama-alumno
    git merge master

    git add 'target/surefire-reports/*.txt'
    git add '*.java'
    git add '*LEEME'
    git add '*.pdf'
    git add '*.txt'
    git add '*.PDF'
    git add '*.TXT'

    
    # rankings
    git add 'time.txt'

    git commit -am "$2"
} > ./prueba.output 2>&1



if [[ $1 = "-h" ]] || [[ $# -ne 2 ]];
then
    echo "Tests disponibles:"
    cat tests | sed 's/^/    /'
    echo ""
    echo "Tienes que introducir entrecomillados el nombre de los tests y un comentario:"
    echo "Ejemplo:"
    echo "    ./prueba.sh \"ayedd.E1.TestSumaEnterosArray\" \"Arreglado código para acarreo\" "
    exit 1
fi





#
# prepare tests
#
{
    rm target/surefire-reports/$1-output.txt
    rm src/test/java/ayedd/*/*java
    TESTS=`echo $1 | sed  's/\./\//g'`
    echo $TESTS
    cp src/test/java/$TESTS.java.orig src/test/java/$TESTS.java


} >> ./prueba.output 2>&1


#
# name of the exercise we want to test
#
EXERCISE=`echo $1 | sed "s/ayedd\.\(.*\)\..*/\1/"`

#
# modify pom.xml to compile only code of requested tests
#
sed -i "s#<include>ayedd/\(.*\)/\(.*\)<\/include>#<include>ayedd/$EXERCISE/\2</include>#" pom.xml

#
# strip package line from *.java if it exists and insert the right one
#
for f in src/main/java/ayedd/$EXERCISE/*.java 
do
    sed -i '/package ayedd.*/d' $f
    sed -i "1 i package ayedd.${EXERCISE};" $f
done

#
# do tests
#
mvn -Dmaven.test.failure.ignore=true -Dtest="$1" test | egrep -v "WARNING"

#
# do i/o tests
#
mvn  exec:exec -Dexec.args="target/surefire-reports/$1-output.txt" | egrep -v "WARNING"




