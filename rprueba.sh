#!/usr/bin/bash

{
    git pull -X theirs origin master
    git pull -X theirs origin rama-alumno

    git checkout rama-alumno
    git merge master

} > ./rprueba.output 2>&1


if [[ $1 = "-h" ]] || [[ $# -ne 2 ]];
then
    echo "Tests disponibles:"
    cat tests | sed 's/^/    /'
    echo ""
    echo "Tienes que introducir entrecomillados el nombre de los tests y un comentario:"
    echo "Ejemplo:"
    echo "    ./rprueba.sh \"ayedd.E1.TestSumaEnterosArray\" \"Arreglado código para acarreo\" "
    exit 1
fi


#
# set TESTS variable in .gitlab-ci.yml pipeline spec
#
cp .gitlab-ci.yml.orig .gitlab-ci.yml
sed -z "s/variables:\n/variables:\n  TESTS: \"$1\"\n/" .gitlab-ci.yml.orig  > .gitlab-ci.yml


#
# name of the exercise we want to test
#
EXERCISE=`echo $1 | sed "s/ayedd\.\(.*\)\..*/\1/"`

#
# modify pom.xml to compile only code of requested tests
#
sed -i "s#<include>ayedd/\(.*\)/\(.*\)<\/include>#<include>ayedd/$EXERCISE/\2</include>#" pom.xml

#
# strip package line from *.java if it exists and insert the right one
#
for f in src/main/java/ayedd/$EXERCISE/*.java 
do
    sed -i '/package ayedd.*/d' $f
    sed -i "1 i package ayedd.${EXERCISE};" $f
done



{
    git add 'target/surefire-reports/*.txt'
    git add '*.java'
    git add '*LEEME'
    git add '*.pdf'
    git add '*.txt'
    git add '*.PDF'
    git add '*.TXT'



    # rankings
    git add 'time.txt'

    git commit -am "$2"
    git push origin rama-alumno
} > ./rprueba.output 2>&1

