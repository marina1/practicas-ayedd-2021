# Sistema de pruebas y entregas de prácticas
## Algoritmos y Estructuras de Datos (AyEdD)
### 1er curso del Grado en Ingeniería de Robótica Software
### Escuela Técnica Superior de Ingeniería de Telecomunicación de la URJC

Este repositorio contiene el software necesario para realizar las
pruebas y entregas de prácticas de la asignatura.

En cada práctica se darán instrucciones del directorio en el que
tienes que colocar tu código para poder probarlo en tu ordenador y
para poder entregarlo.


- **Para probar tus prácticas en tu ordenador** ejecuta el siguiente comando
estando en el directorio raíz de este repositorio:

    ```    ./prueba.sh "Test" "Comentario"```

    ```    Test``` es el nombre de los tests que quieres probar.

    Ejemplo:

    ```    ./prueba.sh "ayedd.E1.TestSumaEnterosArray" "Ya funciona con acarreo" ```


- **Para subir y probar tus prácticas en el servidor** ejecuta el siguiente comando estando en el directorio raíz de este repositorio:

    ```    ./rprueba.sh "Test" "Comentario"```

    ```    Test``` es el nombre de los tests que quieres probar en el servidor

    Ejemplo:

    ```    ./rprueba.sh "ayedd.E1.TestSumaEnterosArray" "Ya funciona con acarreo" ```


    Este comando enviará la última versión de todos tus ficheros terminados en ```.java``` y de cualquier fichero con el nombre ```LEEME```, y ejecutará los tests indicados en el servidor.

- **Para ver qué tests hay disponibles** puedes ejecutar cualquiera de estos dos comandos con la opción ```-h```:

```
    ./prueba.sh -h
       Tests disponibles:
           ayedd.E1.TestSumaEnterosArray
           ayedd.e2.TestSeparador
           ...

    ./rprueba.sh -h
       Tests disponibles:
           ayedd.E1.TestSumaEnterosArray
           ayedd.e2.TestSeparador
           ...
```
